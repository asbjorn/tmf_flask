drop table if exists shopping;
create table shopping (
	id integer primary key autoincrement,
	navn string not null,
	email string not null,
	telefon string not null,
	adresse string not null,
	postkode integer not null,
	land string not null,
	korttype integer not null,
	kortnummer integer not null,
	cv2 integer not null,
	kortnavn string not null,
	apikey integer not null
);
