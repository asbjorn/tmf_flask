#!/usr/bin/env python

from contextlib import closing

import sqlite3
import os
import os.path

from flask import Flask
from flask import render_template
from flask import abort, redirect, url_for
from flask import request, session, g, flash

import urllib
import json as simplejson

from werkzeug.contrib.cache import SimpleCache
cache = SimpleCache()

# database configuration
DATABASE = os.path.join(os.getcwd(),'shopping.db')
DEBUG = True
SECRET_KEY = 'fdsym435324mfrewrsr'
USERNAME = 'admin'
PASSWORD = 'default'

app = Flask(__name__)
app.config.from_object(__name__)
log = app.logger

class APIKeyStore(object):
	def __init__(self):
		self._keys = []
	
	def __len__(self):
		return len(self._keys)

	def add_key(self, key):
		if not key in self._keys:
			log.debug("Added key: %s to keystore." % key)
			self._keys.append(key)
		else:
			log.debug("Key: %s already in keystore." % key)
	
	def is_valid(self, key):
		if not key in self._keys:
			return False
		return True

keystore = APIKeyStore()
with open("keys.txt") as f:
		keys = f.readlines()
		for key in keys:
			key = key.replace('\n','')
			keystore.add_key(key)


def connect_db():
	return sqlite3.connect(app.config['DATABASE'])

def init_db():
	with closing(connect_db()) as db:
		with app.open_resource('db.sql') as f:
			db.cursor().executescript(f.read())
		db.commit()

@app.before_request
def before_request():
	g.db = connect_db()
	g.main_stylesheet = url_for('static', filename="main.css")

@app.teardown_request
def teardown_request(exception):
	g.db.close()

@app.route('/')
def index():
	return render_template("order.html", data={'test':'test data'})


@app.route('/show')
def show_entries():
	cur = g.db.execute('select navn,email,telefon,adresse,postkode,land,korttype,kortnummer,cv2,kortnavn,apikey from shopping order by id desc')
	entries = [dict(navn	= row[0], 
					email	= row[1], 
					telefon	= row[2],
					adresse	= row[3],
					postkode= row[4],
					land	= row[5],
					korttype= row[6],
					kortnr  = row[7],
					cv2		= row[8],
					kortnavn= row[9],
					apikey	= row[10]) for row in cur.fetchall()]
	return render_template('show_entries.html', entries=entries)

@app.route('/add', methods=['POST'])
@app.route('/add/<key>', methods=['POST'])
def ws1(key=None, *args, **kw):
	log.debug('webservice request: args=%s' % request.args)
	log.debug('webservice: arguments=%s, kw=%s' % (args, kw))
	log.debug('webservice form data: %s' % request.form.items())

	if not key:
		key = request.form.get('key', None)
		if not key:
			log.warning("Missing key!")
			return redirect(url_for('index'))

	# we need a valid API key
	if not keystore.is_valid(key):
		log.warning("Key: %s is not valid!" % key)
		return redirect(url_for('index'))

	g.db.execute("""insert into shopping (navn, email, telefon, adresse, postkode, land, korttype, kortnummer, cv2, kortnavn, apikey)
					values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)""",
					(
						request.form['name'],
						request.form['email'],
						request.form['phone'],
						request.form['address'],
						request.form['postcode'],
						request.form['country'],
						request.form['cardtype'],
						request.form['secure'],
						request.form['cardnumber'],
						request.form['namecard'],
						key
					)
				)
	g.db.commit()
	return redirect(url_for('show_entries'))


application = app

if __name__ == "__main__":
	app.debug = True
	app.run(host='0.0.0.0')
